package com.example.izhang.campuslanguageconnection;

import java.util.ArrayList;

/**
 * Created by izhang on 4/13/16.
 */
public class User {
    private String age;
    private String firstName;
    private String lastName;
    private String uniEmail;
    private String uniYear;
    private String verifyCode;
    private String accountId;
    private ArrayList<String> langKnown;
    private ArrayList<String> langLearn;
    private ArrayList<String> interests;
    private ArrayList<User> matches;
    private ArrayList<User> matched;
    private boolean isVerifed = false;
    private boolean canMatch = false;
    private String bucketID = "";

    public User(String age, String firstName, String lastName, String uniEmail, String uniYear, String accountId){
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.uniEmail = uniEmail;
        this.uniYear = uniYear;
        this.accountId = accountId;
        langKnown = new ArrayList<>();
        langLearn = new ArrayList<>();
        interests = new ArrayList<>();
        matches = null;
        matched = new ArrayList<>();
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUniEmail() {
        return uniEmail;
    }

    public void setUniEmail(String uniEmail) {
        this.uniEmail = uniEmail;
    }

    public String getUniYear() {
        return uniYear;
    }

    public void setUniYear(String uniYear) {
        this.uniYear = uniYear;
    }

    public ArrayList<String> getLangKnown() {
        return langKnown;
    }

    public void setLangKnown(ArrayList<String> langKnown) {
        this.langKnown = langKnown;
    }

    public ArrayList<String> getLangLearn() {
        return langLearn;
    }

    public void setLangLearn(ArrayList<String> langLearn) {
        this.langLearn = langLearn;
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<String> interests) {
        this.interests = interests;
    }

    public ArrayList<User> getMatches() {
        return matches;
    }

    public void setMatches(ArrayList<User> matches) {
        this.matches = matches;
    }


    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public boolean isVerifed() {
        return isVerifed;
    }

    public void setIsVerifed(boolean isVerifed) {
        this.isVerifed = isVerifed;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isCanMatch() {
        return canMatch;
    }

    public void setCanMatch(boolean canMatch) {
        this.canMatch = canMatch;
    }

    public String getBucketID() {
        return bucketID;
    }

    public void setBucketID(String bucketID) {
        this.bucketID = bucketID;
    }

    public ArrayList<User> getMatched() {
        return matched;
    }

    public void setMatched(ArrayList<User> matched) {
        this.matched = matched;
    }
}
