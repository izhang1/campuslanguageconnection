package com.example.izhang.campuslanguageconnection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CalendarEventAdapter extends BaseAdapter{
    ArrayList<CalendarEvent> calEventList;
    Context context;
    private static LayoutInflater inflater=null;

    public CalendarEventAdapter(Context context, ArrayList<CalendarEvent> calendarEvents) {
        // TODO Auto-generated constructor stub
        this.calEventList = calendarEvents;
        this.context = context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return calEventList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView;
        rowView = inflater.inflate(R.layout.calendar_list, null);

        CalendarEvent event = calEventList.get(position);
        TextView dateTV = (TextView) rowView.findViewById(R.id.date);
        TextView timeTV = (TextView) rowView.findViewById(R.id.time);
        TextView locationTV = (TextView) rowView.findViewById(R.id.place);
        TextView nameTV = (TextView) rowView.findViewById(R.id.nameMatched);

        dateTV.setText(event.getDate());
        timeTV.setText(event.getTime());
        nameTV.setText(event.getName());
        locationTV.setText(event.getLocation());

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked position: "+  position , Toast.LENGTH_LONG).show();
            }

        });
        return rowView;
    }

}