package com.example.izhang.campuslanguageconnection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MatchedListAdapter extends BaseAdapter {
    ArrayList<User> userList;
    Context context;
    private static LayoutInflater inflater=null;

    public MatchedListAdapter(Context context, ArrayList<User> userList) {
        // TODO Auto-generated constructor stub
        if(userList == null) userList = new ArrayList<User>();
        this.userList = userList;
        this.context = context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView;
        rowView = inflater.inflate(R.layout.matched_list, null);

        User temp = userList.get(position);
        TextView nameMatched = (TextView) rowView.findViewById(R.id.nameMatched);
        TextView emailMatched = (TextView) rowView.findViewById(R.id.emailMasked);
        TextView langLearnMatched = (TextView) rowView.findViewById(R.id.langLearnMatched);
        TextView langKnownMatched = (TextView) rowView.findViewById(R.id.languageKnownMatched);
        TextView interests = (TextView) rowView.findViewById(R.id.interestsMatched);

        nameMatched.setText(temp.getFirstName() + " " + temp.getLastName());
        emailMatched.setText(temp.getUniEmail());
        langLearnMatched.setText(temp.getLangLearn().toString());
        langKnownMatched.setText(temp.getLangKnown().toString());
        interests.setText(temp.getInterests().toString());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked position: " + position, Toast.LENGTH_LONG).show();
            }

        });
        return rowView;
    }

}