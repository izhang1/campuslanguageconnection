package com.example.izhang.campuslanguageconnection;


import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by izhang on 4/12/16.
 */
public class ExpandableListDataPump {

    public static HashMap getLangLearnData(ArrayList<String> list) {
        HashMap expandableListDetail = new HashMap();
        List languageLearning = new ArrayList();

        if(list.size() > 0){
            expandableListDetail.put(list.get(0), languageLearning);

            int count = 1;
            while(count < list.size()){
                languageLearning.add(list.get(count));
                count++;
            }
        }


        return expandableListDetail;
    }

    public static HashMap getLangKnownData(ArrayList<String> list) {
        HashMap expandableListDetail = new HashMap();
        List languageKnown = new ArrayList();

        if(list.size() > 0){
            expandableListDetail.put(list.get(0), languageKnown);

            int count = 1;
            while(count < list.size()){
                languageKnown.add(list.get(count));
                count++;
            }
        }

        return expandableListDetail;
    }

    public static HashMap getInterestData(ArrayList<String> list) {
        HashMap expandableListDetail = new HashMap();
        List interests = new ArrayList();

        if(list.size() > 0){
            expandableListDetail.put(list.get(0), interests);

            int count = 1;
            while(count < list.size()){
                interests.add(list.get(count));
                count++;
            }
        }


        return expandableListDetail;
    }
}