package com.example.izhang.campuslanguageconnection;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;

public class LoginPage extends AppCompatActivity {

    private Button createAcct;
    private Button signIn;
    private AutoCompleteTextView emailText;
    private AutoCompleteTextView passwordText;

    private Firebase firebaseRef;
    private TinyDB tinyDBref;

    private String AGE = "age";
    private String FIRST_NAME = "firstName";
    private String LAST_NAME = "lastName";
    private String PASSWORD = "password";
    private String UNI_EMAIL = "uniEmail";
    private String UNI_YEAR = "uniYear";
    private String IS_VERIFIED = "isverified";
    private String VERIFY_CODE = "verifycode";
    private String LANG_KNOWN = "langKnown";
    private String LANG_LEARN = "langLearn";
    private String INTERESTS = "interests";



    private String android_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        // Get android_id
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        // Firebase Reference
        Firebase.setAndroidContext(this);
        firebaseRef = new Firebase("https://clcdb.firebaseio.com/");

        // TinyDB init
        tinyDBref = new TinyDB(this);

        initUI(this);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmLogin();
            }
        });

    }

    /**
     * Initializes all of the UI for the create account activity
     * @param context
     */
    private void initUI(Context context){
        createAcct = (Button) findViewById(R.id.createaccButton);
        signIn =  (Button) findViewById(R.id.signinButton);
        emailText = (AutoCompleteTextView) findViewById(R.id.emailText);
        passwordText = (AutoCompleteTextView) findViewById(R.id.passwordText);

        createAcct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createAcctIntent = new Intent(getApplicationContext(), CreateAccount.class);
                startActivity(createAcctIntent);
            }
        });

    }

    /**
     * Verifies that the login and password are correct.
     * Direct the users to the main page
     *
     */
    private void confirmLogin(){
        Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                final String userUID = authData.getUid();
                firebaseRef.child("Users").child(userUID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        DataSnapshot temp = dataSnapshot;
                        String age = temp.child(UNI_EMAIL).getValue().toString();
                        String firstName = temp.child(FIRST_NAME).getValue().toString();
                        String lastName = temp.child(LAST_NAME).getValue().toString();
                        String email = temp.child(UNI_EMAIL).getValue().toString();
                        String year = temp.child(UNI_YEAR).getValue().toString();
                        String verifyCode = temp.child(VERIFY_CODE).getValue().toString();

                        ArrayList<String> langKnown = new ArrayList<>();
                        if(temp.child(LANG_KNOWN).exists()) {
                            Iterator langknownIter = temp.child(LANG_KNOWN).getChildren().iterator();
                            while (langknownIter.hasNext()) {
                                DataSnapshot snap = (DataSnapshot)langknownIter.next();
                                langKnown.add(snap.getValue().toString());
                            }
                        }

                        ArrayList<String> langLearn = new ArrayList<>();
                        if(temp.child(LANG_LEARN).exists()) {
                            Iterator langLearnIter = temp.child(LANG_LEARN).getChildren().iterator();
                            while (langLearnIter.hasNext()) {
                                DataSnapshot snap = (DataSnapshot)langLearnIter.next();
                                langLearn.add(snap.getValue().toString());
                            }
                        }

                        ArrayList<String> interestList = new ArrayList<>();
                        if(temp.child(INTERESTS).exists()) {
                            Iterator interestIter = temp.child(INTERESTS).getChildren().iterator();
                            while (interestIter.hasNext()) {
                                DataSnapshot snap = (DataSnapshot)interestIter.next();
                                interestList.add(snap.getValue().toString());
                            }
                        }

                        Log.v("LoginPage", "--> interestList: " + interestList.toString());
                        Log.v("LoginPage", "--> Lang Known: " + langKnown.toString());
                        Log.v("LoginPage", "--> Lang Learn: " + langLearn.toString());


                        boolean isVerified = (boolean) temp.child(IS_VERIFIED).getValue();

                        User user = new User(age, firstName, lastName, email, year, userUID);
                        user.setVerifyCode(verifyCode);
                        user.setLangKnown(langKnown);
                        user.setLangLearn(langLearn);
                        user.setInterests(interestList);
                        user.setIsVerifed(isVerified);
                        tinyDBref.putObject("profile", user);

                        firebaseRef.child("didLogin").child(android_id).setValue(userUID);
                        Intent mainPage = new Intent(getApplicationContext(), HomePage.class);
                        startActivity(mainPage);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });

            }
            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                // Authenticated failed with error firebaseError
            }
        };

        firebaseRef.authWithPassword(emailText.getText().toString(), passwordText.getText().toString(), authResultHandler);

    }

    /**
     * Encrypted password.
     *
     */
    private String encryptPassword(String password){
        // Encrypt the password
        String encryptedPass = "";
        try {
            MessageDigest digester = java.security.MessageDigest.getInstance("MD5");
            digester.update(password.getBytes());
            byte[] hash = digester.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            encryptedPass = hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.v("ErrorRegister", "No Algorithm Exception!");
        }

        return encryptedPass;
    }

}
