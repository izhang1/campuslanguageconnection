package com.example.izhang.campuslanguageconnection;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.andtinder.model.CardModel;
import com.andtinder.model.Orientations;
import com.andtinder.view.CardContainer;
import com.andtinder.view.SimpleCardStackAdapter;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class HomePage extends AppCompatActivity {

    ExpandableListView langKnownExpand;
    ExpandableListAdapter langKnownAdapter;
    List langKnownTitle;
    HashMap langKnownHash;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List expandableListTitle;
    HashMap expandableListDetail;
    Context context;

    ExpandableListView interestExpand;
    ExpandableListAdapter interestAdapter;
    List interestTitle;
    HashMap interestHash;

    private TinyDB tinyDBref;
    private Firebase firebaseRef;
    private Firebase userRef;
    private User profile;
    private int height = 0;
    private int prevHeight = 0;
    private ImageView profilePic;

    // Intent Values
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        context = this;

        // TinyDB Reference
        tinyDBref = new TinyDB(this);

        // Get User Profile
        profile = (User)tinyDBref.getObject("profile", User.class);

        // Firebase Ref
        Firebase.setAndroidContext(this);
        firebaseRef = new Firebase("https://clcdb.firebaseio.com/");
        userRef = firebaseRef.child("Users").child(profile.getAccountId().toString());


        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }


        TabHost tabHost = (TabHost) findViewById(R.id.mainTabHost);
        tabHost.setup();

        TabHost.TabSpec homePage = tabHost.newTabSpec("Home");
        homePage.setContent(R.id.HomePage);
        homePage.setIndicator("Home");
        tabHost.addTab(homePage);

        TabHost.TabSpec matchPage = tabHost.newTabSpec("Match");
        matchPage.setContent(R.id.MatchPage);
        matchPage.setIndicator("Match");
        tabHost.addTab(matchPage);

        TabHost.TabSpec matches = tabHost.newTabSpec("Matches");
        matches.setContent(R.id.MatchedPage);
        matches.setIndicator("List");
        tabHost.addTab(matches);

        TabHost.TabSpec profilePage = tabHost.newTabSpec("Profile");
        profilePage.setContent(R.id.ProfilePage);
        profilePage.setIndicator("Profile");
        tabHost.addTab(profilePage);

        //initMatchesUI();
        initHomeUI();

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                setTitle(tabId);
                if (tabId.equals("Match")) {
                    //initMatchesUI();
                } else if (tabId.equals("Profile")) {
                    initProfileUI();
                }else if(tabId.equals("Matches")){
                    initMatchedUI();
                }
            }
        });

    }

    private void initMatchedUI(){
        ListView matchedList = (ListView) this.findViewById(R.id.matchedList);
        matchedList.setAdapter(new MatchedListAdapter(context, profile.getMatched()));

    }

    private void initHomeUI(){

        firebaseRef.child("Calendar").child("New").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ListView calList = (ListView) findViewById(R.id.calendarList);
                TextView notification = (TextView) findViewById(R.id.notificationView);

                if (!profile.isVerifed()) {
                    notification.setText("Please verify your email to start matching.");
                } else if (profile.getLangLearn().size() < 1) {
                    notification.setText("Please add a language you are currently learning");
                } else {
                    notification.setText("Welcome to CLC. \n We are finding matches for you!");
                }

                ArrayList<CalendarEvent> calEvenetList = new ArrayList<>();
                Iterator calEventIter = dataSnapshot.getChildren().iterator();
                while (calEventIter.hasNext()) {
                    DataSnapshot snap = (DataSnapshot) calEventIter.next();
                    Log.v("HomePage", "--> Cal Event name: " + snap.child("name").toString());
                    Log.v("HomePage", "--> Cal Event date: " + snap.child("date").toString());

                    CalendarEvent temp = new CalendarEvent(snap.child("name").getValue().toString(), snap.child("date").getValue().toString(), snap.child("place").getValue().toString(), snap.child("time").getValue().toString());
                    calEvenetList.add(temp);
                }

                calList.setAdapter(new CalendarEventAdapter(context, calEvenetList));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    private void initMatchesUI(){
        firebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TextView gettingNewMatches = (TextView) findViewById(R.id.newMatches);
                final ProgressBar progressBar = (ProgressBar) findViewById(R.id.loadingBar);
                gettingNewMatches.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);

                final CardContainer mCardContainer = (CardContainer) findViewById(R.id.layoutview);
                mCardContainer.setOrientation(Orientations.Orientation.Ordered);
                final SimpleCardStackAdapter adapter = new SimpleCardStackAdapter(context);
                Log.v("HomePage", "ProfileID: " + profile.getAccountId()) ;

                if(profile.getMatches() == null || profile.getMatches().size() == 0) {
                    Iterator iter = dataSnapshot.child("Matches").child(profile.getBucketID()).getChildren().iterator();
                    Log.v("HomePage", "BucketId: " + profile.getBucketID());
                    if(dataSnapshot.child("Matches").hasChild(profile.getBucketID())){
                        while (iter.hasNext()) {

                            DataSnapshot temp = (DataSnapshot) iter.next();
                            String accountId = (String) temp.getValue();
                            Log.v("HomePage","Inner Account id: " + accountId);
                            DataSnapshot userSnap = dataSnapshot.child("Users").child(accountId);
                            if (!accountId.equals(profile.getAccountId())) {
                                String firstName = userSnap.child("firstName").getValue().toString();
                                String lastName = userSnap.child("lastName").getValue().toString();
                                String age = userSnap.child("age").getValue().toString();
                                String email = userSnap.child("uniEmail").getValue().toString();
                                String year = userSnap.child("uniYear").getValue().toString();

                                ArrayList<String> interests = new ArrayList<String>();
                                for(int i = 0; i < userSnap.child("interests").getChildrenCount(); i++){
                                    interests.add(userSnap.child("interests").child(i + "").getValue().toString());
                                }

                                ArrayList<String> langKnown = new ArrayList<String>();
                                for(int i = 0; i < userSnap.child("langKnown").getChildrenCount(); i++){
                                    langKnown.add(userSnap.child("langKnown").child(i+"").getValue().toString());
                                }

                                ArrayList<String> langLearn = new ArrayList<String>();
                                for(int i = 0; i < userSnap.child("langLearn").getChildrenCount(); i++){
                                    langLearn.add(userSnap.child("langLearn").child(i+"").getValue().toString());
                                }

                                User user = new User(age, firstName, lastName, email, year, accountId);
                                user.setLangKnown(langKnown);
                                user.setLangLearn(langLearn);
                                user.setInterests(interests);

                                profile.getMatches().add(user);

                            }
                        }
                    }

                }

                for(int i = 0; i < profile.getMatches().size(); i++) {
                    User user = profile.getMatches().get(i);
                    final CardModel card = new CardModel(user.getFirstName() + " " + user.getLastName(), "Computer Science" , user.getLangKnown().toString(), user.getLangLearn().toString(),user.getInterests().toString(), context.getDrawable(R.drawable.gravatar));

                    card.setOnCardDismissedListener(new CardModel.OnCardDismissedListener() {
                        @Override
                        public void onLike(String title) {
                            Log.d("Swipeable Card", "Title: " + title);

                            for(int i = 0; i < profile.getMatches().size(); i++){
                                String name = profile.getMatches().get(i).getFirstName() + " " + profile.getMatches().get(i).getLastName();
                                if(name.equals(title)){
                                    if(profile.getMatched() == null) profile.setMatched(new ArrayList<User>());
                                    profile.getMatched().add(profile.getMatches().get(i));
                                    profile.getMatches().remove(i);
                                }
                            }

//                            for(int j = 0; j < profile.getMatched().size(); j++){
//                                Log.v("Swipeable Card", "--> Matched: " + profile.getMatched().get(j).getFirstName());
//                            }

                            for(int p = 0; p < profile.getMatches().size(); p++){
                                Log.v("Swipeable Card", "--> Matched: " + profile.getMatches().get(p).getFirstName());
                            }
                        }

                        @Override
                        public void onDislike(String title) {
                            Log.d("Swipeable Card", "I did not liked it");
                        }
                    });

                    adapter.add(card);

                }

                mCardContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.v("HomePage", "--> Item Click of card");
                    }
                });

                mCardContainer.setAdapter(adapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }


    private void initProfileUI(){
        // Initialize the expandable views on profile page
        initExpandableViews();

        // Init Rest of UI
        TextView nameView = (TextView) findViewById(R.id.nameTextView);
        TextView schoolView = (TextView) findViewById(R.id.SchooTextView);
        TextView yearView = (TextView) findViewById(R.id.yearTextView);
        TextView emailView = (TextView) findViewById(R.id.emailTextView);
        Button verifyButton = (Button) findViewById(R.id.verifyButton);
        final Button addLangKnown = (Button) findViewById(R.id.addLangKnown);
        Button addLangLearn = (Button) findViewById(R.id.addLangLearn);
        Button addInterests = (Button) findViewById(R.id.addInterests);
        profilePic = (ImageView) findViewById(R.id.imageView);

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        nameView.setText(profile.getFirstName() + " " + profile.getLastName());
        schoolView.setText("Purdue University");
        yearView.setText(profile.getUniYear());
        emailView.setText(profile.getUniEmail());

        if(profile.isVerifed()){
            verifyButton.setVisibility(View.INVISIBLE);
        }

        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyEmail();
            }
        });

        addLangKnown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewLanguageKnown();
            }
        });

        addLangLearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewLanguagesLearning();
            }
        });

        addInterests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewInterests();
                //saveProfile();
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 350, 450, false);
            profilePic.setImageBitmap(imageBitmap);

            tinyDBref.putImageWithFullPath(getApplication().getPackageCodePath() + "/image", imageBitmap);
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void addNewInterests(){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Add New Interest");

        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_addinterests, null);
        alert.setView(dialogView);

        final EditText input = (EditText) dialogView.findViewById(R.id.codeInput);

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                profile.getInterests().add(input.getText().toString());
                initExpandableViews();
               for(int i = 0; i < profile.getInterests().size(); i++){
                    userRef.child("interests").child(Integer.toString(i)).setValue(profile.getInterests().get(i));
                }

            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        alert.show();
    }

    private int selectedPos;

    private void addNewLanguagesLearning(){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Add Learning Language");

        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_languages, null);
        alert.setView(dialogView);

        final ListView langList = (ListView) dialogView.findViewById(R.id.langList);

        final ArrayList<String> languages = new ArrayList<>();
        languages.add("Chinese");
        languages.add("English");

        ArrayAdapter langListAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_checked, languages);
        langList.setAdapter(langListAdapter);
        langList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        langList.setItemChecked(0, true);

        langList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPos = position;
                langList.setItemChecked(position, true);
                langList.setSelection(position);
            }
        });

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.v("HomePage", "--> Add Lang Known : " + " Selected Position: " + selectedPos);
                String language = languages.get(selectedPos);
                if(!profile.getLangLearn().contains(language)) {

                    profile.getLangLearn().add(language);
                    initExpandableViews();
                    saveProfile();


                    // Add to firebase
                    for (int i = 0; i < profile.getLangLearn().size(); i++) {
                        userRef.child("langLearn").child(Integer.toString(i)).setValue(profile.getLangLearn().get(i));
                    }
                }else{
                    Toast.makeText(context, "Language already added", Toast.LENGTH_LONG).show();
                }

                selectedPos = 0;
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        alert.show();
    }

    private void addNewLanguageKnown(){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Add Language Known");

        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_languages, null);
        alert.setView(dialogView);

        final ListView langList = (ListView) dialogView.findViewById(R.id.langList);

        final ArrayList<String> languages = new ArrayList<>();
        languages.add("Chinese");
        languages.add("English");

        ArrayAdapter langListAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_checked, languages);
        langList.setAdapter(langListAdapter);
        langList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        langList.setItemChecked(0, true);

        langList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPos = position;
                langList.setItemChecked(position, true);
                langList.setSelection(position);
            }
        });

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.v("HomePage", "--> Add Lang Known : " + " Selected Postition: " + selectedPos);
                String language = languages.get(selectedPos);
                if(!profile.getLangKnown().contains(language)) {
                    profile.getLangKnown().add(language);
                    initExpandableViews();
                    saveProfile();

                    // Add to firebase
                    for (int i = 0; i < profile.getLangKnown().size(); i++) {
                        userRef.child("langKnown").child(Integer.toString(i)).setValue(profile.getLangKnown().get(i));
                    }
                }

                selectedPos = 0;
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        alert.show();
    }

    private void verifyEmail(){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Verify Email Code");

        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.dialog_verifyemail, null);
        alert.setView(dialogView);
        final EditText codeInput = (EditText) dialogView.findViewById(R.id.codeInput);

        alert.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.v("HomePage", "--> Verify Code: " + profile.getVerifyCode());
                if(codeInput.getText().toString().equals(profile.getVerifyCode())){
                    profile.setIsVerifed(true);
                    saveProfile();
                    Button verifyButton = (Button) findViewById(R.id.verifyButton);
                    verifyButton.setVisibility(View.INVISIBLE);
                    userRef.child("isverified").setValue(true);
                    Toast.makeText(getApplicationContext(), "Email has been verified!", Toast.LENGTH_LONG).show();

                }
            }
        });

        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        alert.show();
    }

    private void initExpandableViews(){
         /*
         * Language Known Expandable View
         */
        langKnownExpand = (ExpandableListView) findViewById(R.id.langKnownExpand);
        langKnownHash = ExpandableListDataPump.getLangKnownData(profile.getLangKnown());
        langKnownTitle = new ArrayList(langKnownHash.keySet());
        langKnownAdapter = new ExpandableListAdapter(this, langKnownTitle, langKnownHash);
        langKnownExpand.setAdapter(langKnownAdapter);
        langKnownExpand.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < langKnownExpand.getChildCount(); i++) {
                    height += langKnownExpand.getChildAt(i).getMeasuredHeight();
                    if (langKnownExpand.getChildCount() != 1)
                        height += langKnownExpand.getDividerHeight();
                }

                langKnownExpand.getLayoutParams().height = height * (langKnownAdapter.getChildrenCount(0) + 1);
                prevHeight = height * (langKnownAdapter.getChildrenCount(0) + 1);
                height = 0;
            }
        });


        langKnownExpand.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                for (int i = 1; i < langKnownExpand.getChildCount(); i++) {
                    height += langKnownExpand.getChildAt(i).getMeasuredHeight();
                    height += langKnownExpand.getDividerHeight();
                }
                if(height == 0 && langKnownExpand.getChildCount() == 1){

                }else{
                    langKnownExpand.getLayoutParams().height = prevHeight - height * (langKnownAdapter.getChildrenCount(0) + 1);
                }
                height = 0;
                prevHeight = 0;
                //langKnownExpand.getLayoutParams().height = prevHeight;
            }
        });


        /*
         * Language Learning Expandable View
         */
        expandableListView = (ExpandableListView) findViewById(R.id.langLearnExpand);
        expandableListDetail = ExpandableListDataPump.getLangLearnData(profile.getLangLearn());
        expandableListTitle = new ArrayList(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < expandableListView.getChildCount(); i++) {
                    height += expandableListView.getChildAt(i).getMeasuredHeight();
                    if (expandableListView.getChildCount() != 1)
                        height += expandableListView.getDividerHeight();
                }
                Log.v("HomePage", "--> Group Count: " + expandableListAdapter.getGroupCount());
                Log.v("HomePage", "---> Children Count: " + expandableListAdapter.getChildrenCount(0));

                expandableListView.getLayoutParams().height = height * (expandableListAdapter.getChildrenCount(0) + 1);
                prevHeight = height * (expandableListAdapter.getChildrenCount(0) + 1);
                height = 0;
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                for (int i = 1; i < expandableListView.getChildCount(); i++) {
                    height += expandableListView.getChildAt(i).getMeasuredHeight();
                    height += expandableListView.getDividerHeight();
                }
                if(height == 0 && expandableListView.getChildCount() == 1){

                }else{
                    expandableListView.getLayoutParams().height = prevHeight - height * (expandableListAdapter.getChildrenCount(0) + 1);
                }
                height = 0;
                prevHeight = 0;
                //langKnownExpand.getLayoutParams().height = prevHeight;
            }
        });


        /*
         * Language Known Expandable View
         */

        interestExpand = (ExpandableListView) findViewById(R.id.interestsExpand);
        interestHash = ExpandableListDataPump.getInterestData(profile.getInterests());
        interestTitle = new ArrayList(interestHash.keySet());
        interestAdapter = new ExpandableListAdapter(this, interestTitle, interestHash);
        interestExpand.setAdapter(interestAdapter);
        interestExpand.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < interestExpand.getChildCount(); i++) {
                    height += interestExpand.getChildAt(i).getMeasuredHeight();
                    if (interestExpand.getChildCount() != 1)
                        height += interestExpand.getDividerHeight();
                }

                interestExpand.getLayoutParams().height = height * (interestAdapter.getChildrenCount(0) + 1);
                prevHeight = height * (interestAdapter.getChildrenCount(0) + 1);
                height = 0;
            }
        });

        interestExpand.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                for (int i = 1; i < interestExpand.getChildCount(); i++) {
                    height += interestExpand.getChildAt(i).getMeasuredHeight();
                    height += interestExpand.getDividerHeight();
                }
                if(height == 0 && interestExpand.getChildCount() == 1){

                }else{
                    interestExpand.getLayoutParams().height = prevHeight - height * (interestAdapter.getChildrenCount(0) + 1);
                }
                height = 0;
                prevHeight = 0;
                //langKnownExpand.getLayoutParams().height = prevHeight;
            }
        });

    }

    private void saveProfile(){
        checkRequirements();
        tinyDBref.remove("profile");
        tinyDBref.putObject("profile", profile);
    }

    private void checkRequirements(){

        if(profile.isCanMatch()) return;

        if(profile.isVerifed() == true && profile.getLangKnown().size() >= 1 && profile.getLangLearn().size() >= 1){
            profile.setCanMatch(true);
            if(profile.getLangKnown().size() == 2){
                if(profile.getLangLearn().size() == 1 && profile.getLangKnown().get(0).equals("English")){
                    firebaseRef.child("Matches").child("LEKCE").child(profile.getAccountId()).setValue(profile.getAccountId());
                    profile.setBucketID("LEKCE");
                }else{
                    firebaseRef.child("Matches").child("LCKCE").child(profile.getAccountId()).setValue(profile.getAccountId());
                    profile.setBucketID("LCKCE");
                }
            }else{
                firebaseRef.child("Matches").child("LEKC").child(profile.getAccountId()).setValue(profile.getAccountId());
                profile.setBucketID("LEKC");
            }
            firebaseRef.child("Users").child(profile.getAccountId()).child("canMatch").setValue(true);
            saveProfile();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        tinyDBref.remove("profile");
        tinyDBref.putObject("profile", profile);
    }

}
