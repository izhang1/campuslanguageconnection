package com.example.izhang.campuslanguageconnection;

/**
 * Created by izhang on 4/20/16.
 */
public class CalendarEvent {
    private String name;
    private String date;
    private String location;
    private String time;

    CalendarEvent(String name, String date, String location, String time){
        this.name = name;
        this.date = date;
        this.location = location;
        this.time = time;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
