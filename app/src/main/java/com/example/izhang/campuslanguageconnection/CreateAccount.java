package com.example.izhang.campuslanguageconnection;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.mail.*;

import javax.mail.internet.*;

import com.sun.mail.smtp.*;

import java.io.*;
import java.net.InetAddress;
import java.util.Map;
import java.util.Properties;
import java.util.Date;


public class CreateAccount extends AppCompatActivity {

    private AutoCompleteTextView nameText;
    private AutoCompleteTextView uniEmailText;
    private AutoCompleteTextView ageText;
    private AutoCompleteTextView passwordText;
    private AutoCompleteTextView verifyPasswordText;
    private Spinner year;
    private TextView terms;
    String[] schoolYears = {"Freshmen", "Sophomore", "Junior", "Senior", "Graduate"};

    private Context context;
    private Firebase firebaseRef;

    private String AGE = "age";
    private String FIRST_NAME = "firstName";
    private String LAST_NAME = "lastName";
    private String PASSWORD = "password";
    private String UNI_EMAIL = "uniEmail";
    private String UNI_YEAR = "uniYear";
    private String IS_VERIFIED = "isverified";
    private String VERIFY_CODE = "verifycode";

    private TinyDB tinyDBref;
    private String android_id;

    private long verifyCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        context = this;

        // Firebase init for account
        Firebase.setAndroidContext(this);
        firebaseRef = new Firebase("https://clcdb.firebaseio.com/");

        // TinyDB init
        tinyDBref = new TinyDB(context);

        // Get android_id
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        Window window = getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }


        initUI(context);

        Button continueButton = (Button) findViewById(R.id.continueButton);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyEduEmail() && verifyInput() && verifyPassword()) {
                    verifyCode = Math.round(Math.random() * 100000);
                    sendEmailConfirmation();
                    saveToFirebase();
                }
            }
        });

    }



    /**
     * Initializes all of the UI for the create account activity
     * @param context
     */
    private void initUI(Context context){
        nameText = (AutoCompleteTextView) findViewById(R.id.nameTextView);
        uniEmailText = (AutoCompleteTextView) findViewById(R.id.uniEmailText);
        ageText = (AutoCompleteTextView) findViewById(R.id.ageText);
        passwordText = (AutoCompleteTextView) findViewById(R.id.passwordText);
        verifyPasswordText = (AutoCompleteTextView) findViewById(R.id.verifyPasswordText);
        terms = (TextView) findViewById(R.id.termsText);

        year = (Spinner)findViewById(R.id.yearSpinner);

        ArrayAdapter yearAdapter = new ArrayAdapter(context ,R.layout.support_simple_spinner_dropdown_item, schoolYears);
        year.setAdapter(yearAdapter);

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTerms();
            }
        });
    }



    /**
     * Verifies the user is putting in an EDU email.
     *
     * @return
     */
    private boolean verifyEduEmail(){
        String uniEmail = uniEmailText.getText().toString();
        if(uniEmail.contains(".edu") && uniEmail.contains("purdue") || uniEmail.contains("northwestern")){
            return true;
        }else{
            Toast.makeText(getApplication(), "Please enter an .edu email", Toast.LENGTH_LONG).show();
            return false;
        }
    }



    /**
     * Verifies the user is inputting the right name
     *
     */
    private boolean verifyInput(){
        String []name = nameText.getText().toString().split(" ");
        if(name[0].isEmpty() || name[1].isEmpty()){
            Toast.makeText(getApplication(), "Please enter a first and last name. Ex) Andrew Yote", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }



    /**
     * Verifies the passwords are the same and have a certain length
     *
     */
    private boolean verifyPassword(){
        String pass1 = passwordText.getText().toString();
        String pass2 = verifyPasswordText.getText().toString();
        if(pass1.length() < 5){
            Toast.makeText(getApplication(), "Password length is too short. Please enter a longer one.", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pass1.equals(pass2)){
            return true;
        }else{
            Toast.makeText(getApplication(), "The passwords do not match", Toast.LENGTH_LONG).show();
            return false;
        }
    }



    /**
     * Shows the terms to the user via a AlertDialog
     */
    private void showTerms(){
        // Create action window to make lockcode
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Terms of Use");

        alert.setMessage("In order to use CLC, we will be doing some things that you need to be aware of. Checking the box indicates that you accept these terms.");

        alert.setNegativeButton("Return",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
        alert.show();
    }



    /**
     *  starts the email confirmation asynctask to user
     *
     */
    private void sendEmailConfirmation(){
        sendEmailMessage smtpThread = new sendEmailMessage(uniEmailText.getText().toString());
        smtpThread.execute();
    }



    /**
     *  Save all inputted data into firebase
     *
     */
    private void saveToFirebase(){

        String email = uniEmailText.getText().toString();
        String password = passwordText.getText().toString();
        firebaseRef.createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> result) {
                Log.v("CreateAccount","Successfully created user account with uid: " + result.get("uid") );
                System.out.println("Successfully created user account with uid: " + result.get("uid"));

                // Send the data into Firebase
                Firebase userFirebase = firebaseRef.child("Users").child(result.get("uid").toString());
                String temp[] = parseName(nameText.getText().toString());
                userFirebase.child(AGE).setValue(ageText.getText().toString());
                userFirebase.child(FIRST_NAME).setValue(temp[0]);
                userFirebase.child(LAST_NAME).setValue(temp[1]);
                userFirebase.child(UNI_EMAIL).setValue(uniEmailText.getText().toString());
                userFirebase.child(UNI_YEAR).setValue(year.getSelectedItem().toString());
                userFirebase.child(IS_VERIFIED).setValue(false);
                userFirebase.child(VERIFY_CODE).setValue(verifyCode);

                firebaseRef.child("didLogin").child(android_id).setValue(result.get("uid").toString());

                User user = new User(ageText.getText().toString(), temp[0], temp[1], uniEmailText.getText().toString(), year.getSelectedItem().toString(), result.get("uid").toString());
                user.setVerifyCode(""+verifyCode);
                user.setIsVerifed(false);
                tinyDBref.putObject("profile", user);

                // Immediately go to MainPage
                Intent profilePage = new Intent(getApplication(), HomePage.class);
                startActivity(profilePage);
                finish();
            }
            @Override
            public void onError(FirebaseError firebaseError) {
                Log.v("CreateAccount","Error has occured " + firebaseError.toString());
                Toast.makeText(context, firebaseError.toString(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private String[] parseName(String name){
        String temp[] = name.split(" ");
        return temp;
    }

    private String encryptPassword(String password){
        // Encrypt the password
        String encryptedPass = "";
        try {
            MessageDigest digester = java.security.MessageDigest.getInstance("MD5");
            digester.update(password.getBytes());
            byte[] hash = digester.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            encryptedPass = hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.v("ErrorRegister", "No Algorithm Exception!");
        }

        return encryptedPass;
    }


    /**
     * Asynctask that sends the email message with verification code to user.
     *
     */
    class sendEmailMessage extends AsyncTask<String, Void, Void> {

        private String email;

        sendEmailMessage(String email){
            this.email = email;
        }

        protected Void doInBackground(String... urls) {
            Properties props = System.getProperties();
            props.put("mail.smtps.host","smtp.mailgun.org");
            props.put("mail.smtps.auth","true");
            Session session = Session.getInstance(props, null);
            Message msg = new MimeMessage(session);
            try {
                msg.setFrom(new InternetAddress("verify@clc.com"));
                msg.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(email, false));
                msg.setSubject("CLC Verification");
                msg.setText("Please verify using this code: " + verifyCode);
                msg.setSentDate(new Date());
                SMTPTransport t =
                        (SMTPTransport) session.getTransport("smtps");
                t.connect("smtp.mailgun.com", "postmaster@sandbox1c4f568a3d824082aeb584a77ad52d87.mailgun.org", "6fe81c887db71257378dc67542b07b85");
                t.sendMessage(msg, msg.getAllRecipients());
                System.out.println("Response: " + t.getLastServerResponse());
                t.close();
            }catch(MessagingException e){
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute() {
            // TODO: check this.exception
            // TODO: do something with the feed
        }
    }

}
