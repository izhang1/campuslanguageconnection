/**
 * AndTinder v0.1 for Android
 *
 * @Author: Enrique López Mañas <eenriquelopez@gmail.com>
 * http://www.lopez-manas.com
 *
 * TAndTinder is a native library for Android that provide a
 * Tinder card like effect. A card can be constructed using an
 * image and displayed with animation effects, dismiss-to-like
 * and dismiss-to-unlike, and use different sorting mechanisms.
 *
 * AndTinder is compatible with API Level 13 and upwards
 *
 * @copyright: Enrique López Mañas
 * @license: Apache License 2.0
 */

package com.andtinder.model;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class CardModel {

	private String title;
	private String langLearning;
    private String langKnown;
    private String interests;
    private String major;
	private Drawable cardImageDrawable;
	private Drawable cardLikeImageDrawable;
	private Drawable cardDislikeImageDrawable;

    private OnCardDismissedListener mOnCardDismissedListener = null;

    private OnClickListener mOnClickListener = null;


    public interface OnCardDismissedListener {
        void onLike(String title);
        void onDislike(String title);
    }

    public interface OnClickListener {
        void OnClickListener();
    }

	public CardModel() {
		this(null, null, null, null, null,(Drawable)null);
	}

	public CardModel(String title, String major, String langLearning, String langKnown, String interests, Drawable cardImage) {
		this.title = title;
        this.langLearning = langLearning;
        this.langKnown = langKnown;
        this.interests = interests;
		this.major = major;
		this.cardImageDrawable = cardImage;
	}

	public CardModel(String title, String major,String langLearning, String langKnown, String interests, Bitmap cardImage) {
		this.title = title;
        this.langLearning = langLearning;
        this.langKnown = langKnown;
        this.interests = interests;
		this.major = major;
		this.cardImageDrawable = new BitmapDrawable(null, cardImage);
	}

    public String getLangLearning() {
        return langLearning;
    }

    public void setLangLearning(String langLearning) {
        this.langLearning = langLearning;
    }

    public String getLangKnown() {
        return langKnown;
    }

    public void setLangKnown(String langKnown) {
        this.langKnown = langKnown;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMajor() {
		return major;
	}

	public void setDescription(String description) {
		this.major = description;
	}

	public Drawable getCardImageDrawable() {
		return cardImageDrawable;
	}

	public void setCardImageDrawable(Drawable cardImageDrawable) {
		this.cardImageDrawable = cardImageDrawable;
	}

	public Drawable getCardLikeImageDrawable() {
		return cardLikeImageDrawable;
	}

	public void setCardLikeImageDrawable(Drawable cardLikeImageDrawable) {
		this.cardLikeImageDrawable = cardLikeImageDrawable;
	}

	public Drawable getCardDislikeImageDrawable() {
		return cardDislikeImageDrawable;
	}

	public void setCardDislikeImageDrawable(Drawable cardDislikeImageDrawable) {
		this.cardDislikeImageDrawable = cardDislikeImageDrawable;
	}

    public void setOnCardDismissedListener( OnCardDismissedListener listener ) {
        this.mOnCardDismissedListener = listener;
    }

    public OnCardDismissedListener getOnCardDismissedListener() {
       return this.mOnCardDismissedListener;
    }


    public void setOnClickListener( OnClickListener listener ) {
        this.mOnClickListener = listener;
    }

    public OnClickListener getOnClickListener() {
        return this.mOnClickListener;
    }
}
